package com.huawei.projectman.demo;

import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.ProjectManClient;
import com.huaweicloud.sdk.projectman.v4.model.CreateIssueRequestV4;
import com.huaweicloud.sdk.projectman.v4.model.CreateIssueV4Request;
import com.huaweicloud.sdk.projectman.v4.model.CreateIssueV4Response;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectManAddIssuesDemo {
    private static final Logger logger = LoggerFactory.getLogger(ProjectManAddIssuesDemo.class.getName());

    public static void main(String[] args) {
        String ak = "{ak}";
        String sk = "{sk}";
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.valueOf(ProjectManRegion.CN_EAST_2.getId()))
                .build();
        CreateIssueV4Request request = new CreateIssueV4Request();

        request.setProjectId("{your project_id}");
        CreateIssueRequestV4 body = new CreateIssueRequestV4();
        body.setName("demo");
        body.setPriorityId(1); // 1：low
        body.setTrackerId(7); // 7:stroy

        request.withBody(body);
        try {
            CreateIssueV4Response response = client.createIssueV4(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            logger.error("connection error",e);
        } catch (RequestTimeoutException e) {
            logger.error("RequestTimeoutException",e);
        } catch (ServiceResponseException e) {
            logger.error("ServiceResponseException",e);
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
