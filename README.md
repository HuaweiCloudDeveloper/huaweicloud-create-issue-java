## 1.版本说明
本示例基于华为云SDK V3.0+版本开发。 
## 2.功能介绍
华为云提供了项目管理SDK，您可以直接集成服务端SDK来调用项目管理SDK的相关API，从而实现对项目管理服务的快速操作。 该示例展示了如何通过Java版SDK创建scrum项目工作项。
## 3.前置条件
- 1.开发者已注册华为云账号。
- 2.已创建scrum类型项目。
- 3.开发者在使用前需先获取账号的**AK、SK**。
## 4.安装SDK
您可以通过Maven方式获取和安装SDK，您只需要在Java项目的pom.xml文件中加入相应的依赖项即可。
ProjectMan的sdk版本需高于3.0.0，具体的SDK版本号请参见[SDK开发中心](https://sdkcenter.developer.huaweicloud.com/?product=%E7%BC%96%E8%AF%91%E6%9E%84%E5%BB%BA)。
```java
<dependencies>
   <dependency>
     <groupId>com.huaweicloud.sdk</groupId>
     <artifactId>huaweicloud-sdk-projectman</artifactId>
     <version>3.1.18</version>
   </dependency>
</dependencies>
```
## 5.示例代码
```java
import com.huaweicloud.sdk.core.auth.ICredential;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.exception.ConnectionException;
import com.huaweicloud.sdk.core.exception.RequestTimeoutException;
import com.huaweicloud.sdk.core.exception.ServiceResponseException;
import com.huaweicloud.sdk.projectman.v4.region.ProjectManRegion;
import com.huaweicloud.sdk.projectman.v4.*;
import com.huaweicloud.sdk.projectman.v4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProjectManAddIssuesDemo {
    private static final Logger logger = LoggerFactory.getLogger(ProjectManAddIssuesDemo.class.getName());

    public static void main(String[] args) {
        String ak = "{ak}";
        String sk = "{sk}";
        ICredential auth = new BasicCredentials()
                .withAk(ak)
                .withSk(sk);

        ProjectManClient client = ProjectManClient.newBuilder()
                .withCredential(auth)
                .withRegion(ProjectManRegion.valueOf(ProjectManRegion.CN_EAST_2.getId()))
                .build();
        CreateIssueV4Request request = new CreateIssueV4Request();

        request.setProjectId("{your project_id}");
        CreateIssueRequestV4 body = new CreateIssueRequestV4();
        body.setName("demo");
        body.setPriorityId(1); // 1：low
        body.setTrackerId(7); // 7:stroy

        request.withBody(body);
        try {
            CreateIssueV4Response response = client.createIssueV4(request);
            System.out.println(response.toString());
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (RequestTimeoutException e) {
            e.printStackTrace();
        } catch (ServiceResponseException e) {
            e.printStackTrace();
            System.out.println(e.getHttpStatusCode());
            System.out.println(e.getErrorCode());
            System.out.println(e.getErrorMsg());
        }
    }
}
```
## 6.修订记录
|发布日期  |版本号  |修订说明  |
|--|--|--|
| 2022.12.23 | 1.1 |首次发布  |